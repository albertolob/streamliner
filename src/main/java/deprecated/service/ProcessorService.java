package deprecated.service;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.Vertx;

@ProxyGen
public interface ProcessorService {

    static ProcessorService create(final Vertx vertx, final String address) {
        return new ProcessorServiceImpl(vertx, address);
    }

    <C> void receive(C command);
}
