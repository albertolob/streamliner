package io.streamliner.eventsourcing.clustering;

import io.vertx.core.AbstractVerticle;

import java.util.UUID;

public abstract class SingletonVerticle extends AbstractVerticle {

    private final String address = UUID.randomUUID().toString();

    public final String getAddress() {
        return address;
    }
}
