package io.streamliner.eventsourcing.clustering;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.AsyncMap;

public class ClusteredVerticle extends AbstractVerticle {

    private static final Logger logger = LoggerFactory.getLogger(ClusteredVerticle.class);
    private AsyncMap<String, String> sharedData;
    private AbstractVerticle verticle;
    private final String publicAddress;
    private final String privateAddress;
    private String routingAddress;
    private String deploymentId;

    public ClusteredVerticle(SingletonVerticle verticle, String publicAddress) {

        this.verticle = verticle;
        this.publicAddress = publicAddress;
        this.privateAddress = verticle.getAddress();
    }

    public void start() {

        logger.info("Starting verticle " + getClass().getName());
        logger.info("Registering shared data at: " + "es.processors");
        vertx.sharedData().<String, String>getClusterWideMap("es.processors", response1 -> {
            if (response1.succeeded()) {
                sharedData = response1.result();
                deploy(1);
            } else {
                logger.error(response1.cause().getMessage());
                throw new RuntimeException(response1.cause());
            }
        });

        logger.info("Registering event bus at: " + publicAddress);
        vertx.eventBus().consumer(publicAddress, message -> {
            logger.info("Received message at: " + publicAddress);
            route(message, 1);
        });
        logger.info("Verticle " + getClass().getName() + " started");
    }

    private void route(final Message message, final int retries) {

        sharedData.get(publicAddress, response1 -> {
            if (response1.succeeded()) {
                final String address = response1.result();
                logger.info("Routing message to: " + address);
                vertx.eventBus().send(address, message.body(), response2 -> {
                    if (response2.succeeded()) {
                        logger.info("Successfully routing message to: " + address);
                        message.reply(response2.result().body());
                    } else {
                        logger.error("Failing routing message to: " + address);
                        if (retries <= 3) {
                            route(message, retries + 1);
                        } else {
                            deploy(1);
                        }
                    }
                });
            } else {
                logger.error("Failing to query shared data for: " + publicAddress);
                if (retries <= 3) {
                    route(message, retries + 1);
                } else {
                    deploy(1);
                }
            }
        });
    }

    @Override
    public void stop() {

        logger.info("Stopping verticle" + getClass().getName());
        undeploy(1);
        logger.info("Verticle " + getClass().getName() + " stopped");
    }

    private void deploy(final int retries) {

        logger.info("Deploying verticle " + getClass().getName());
        sharedData.get(publicAddress, response1 -> {
            if (response1.succeeded()) {
                final String address = response1.result();
                if (address == null) {
                    sharedData.putIfAbsent(publicAddress, privateAddress, response2 -> {
                        if (response2.succeeded()) {
                            vertx.deployVerticle(verticle, response3 -> {
                                if (response3.succeeded()) {
                                    logger.info("Verticle " + getClass().getName() + " deployed");
                                    logger.info(privateAddress + " is master for: " + publicAddress);
                                    deploymentId = response3.result();
                                } else {
                                    logger.error(response3.cause().getMessage());
                                    throw new RuntimeException(response3.cause());
                                }
                            });
                        } else {
                            logger.warn(response2.cause().getMessage());
                            if (retries <= 3) {
                                deploy(retries + 1);
                            } else {
                                logger.error(response2.cause().getMessage());
                                throw new RuntimeException(response1.cause());
                            }
                        }
                    });
                } else {
                    if (routingAddress == null) {
                        logger.info(privateAddress + " is slave for: " + publicAddress);
                        routingAddress = address;
                    } else {
                        if (routingAddress.equals(address)) {
                            sharedData.remove(publicAddress, response3 -> {
                                if (response3.succeeded()) {
                                    deploy(1);
                                } else {
                                    logger.warn(response3.cause().getMessage());
                                    if (retries <= 3) {
                                        deploy(retries + 1);
                                    } else {
                                        logger.error(response3.cause().getMessage());
                                        throw new RuntimeException(response3.cause());
                                    }
                                }
                            });
                        }
                    }
                }
            } else {
                logger.warn(response1.cause().getMessage());
                if (retries <= 3) {
                    deploy(retries + 1);
                } else {
                    logger.error(response1.cause().getMessage());
                    throw new RuntimeException(response1.cause());
                }
            }
        });
    }

    private void undeploy(final int retries) {

        logger.info("Undeploying verticle " + getClass().getName());
        sharedData.get(publicAddress, response1 -> {
            if (response1.succeeded()) {
                final String address = response1.result();
                if (address != null && address.equals(privateAddress)) {
                    sharedData.remove(address, response2 -> {
                        if (response2.succeeded()) {
                            if (deploymentId != null) {
                                vertx.undeploy(deploymentId);
                            }
                            logger.info("Verticle " + getClass().getName() + " undeployed");
                        } else {
                            logger.warn(response2.cause().getMessage());
                            if (retries <= 3) {
                                undeploy(retries + 1);
                            } else {
                                logger.error(response2.cause().getMessage());
                                throw new RuntimeException(response1.cause());
                            }
                        }
                    });
                }
            } else {
                logger.warn(response1.cause().getMessage());
                if (retries <= 3) {
                    undeploy(retries + 1);
                } else {
                    logger.error(response1.cause().getMessage());
                    throw new RuntimeException(response1.cause());
                }
            }
        });
    }
}
