/**
 * Indicates that this module contains classes that need to be generated / processed.
 */
@ModuleGen(name = "vertx-event-sourcing", groupPackage = "io.streamliner.eventsourcing.processor")
package io.streamliner.eventsourcing;

import io.vertx.codegen.annotations.ModuleGen;