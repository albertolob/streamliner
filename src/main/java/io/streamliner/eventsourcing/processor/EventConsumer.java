package io.streamliner.eventsourcing.processor;

import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.persistence.PersistenceType;

public class EventConsumer<T> extends AbstractConsumer<T> {

    private static final String PERSISTENCE_TYPE = PersistenceType.EVENT;

    public EventConsumer(final EventStore<T> store, final String persistenceId, final String clientId) {
        super(store, persistenceId, PERSISTENCE_TYPE, clientId);
    }
}
