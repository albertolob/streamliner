package io.streamliner.eventsourcing.processor;

import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.messaging.protocol.Offset;
import io.streamliner.eventsourcing.streaming.ConsumerTask;
import io.streamliner.eventsourcing.streaming.InfiniteStream;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;

import java.util.stream.Stream;

public class AbstractConsumer<T> extends AbstractVerticle {

    private final EventStore<T> store;
    private final String persistenceId;
    private final String persistenceType;
    private final String clientId;
    private final ConsumerTask<T> consumerTask;

    public AbstractConsumer(final EventStore<T> store, final String persistenceId, final String persistenceType, final String clientId) {
        this.store = store;
        this.persistenceId = persistenceId;
        this.persistenceType = persistenceType;
        this.clientId = clientId;
        this.consumerTask = new ConsumerTask<T>(store, getPersistenceId(), getPersistenceType(), getClientId());
    }

    public String getPersistence() {

        return store.getPersistence();
    }

    public String getPersistenceType() {

        return persistenceType;
    }

    public String getPersistenceId() {

        return persistenceId;
    }

    public String getClientId() {

        return clientId;
    }

    @Override
    public void start() {

        final String key = getPersistence() + "->" + getPersistenceId() + "->" + getClientId();
        ServiceDiscovery discovery = ServiceDiscovery.create(vertx,
                new ServiceDiscoveryOptions().setAnnounceAddress("es.client").setName(key));
    }

    public Stream<Envelope<T>> getStream() {

        consumerTask.start();
        return new InfiniteStream<T>(consumerTask.getQueueing());
    }

    public void acknowledge(final Offset offset, final Handler<Offset> handler) {
        store.acknowledge(getPersistenceId(), getPersistenceType(), getClientId(), offset.getPartitionNumber(), offset.getSequenceNumber(), handler);
    }

    public void close() {
        consumerTask.stop();
    }

}
