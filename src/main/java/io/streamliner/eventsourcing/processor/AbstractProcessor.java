package io.streamliner.eventsourcing.processor;

import io.streamliner.eventsourcing.clustering.SingletonVerticle;
import io.streamliner.eventsourcing.persistence.*;
import io.streamliner.eventsourcing.messaging.Sender;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public abstract class AbstractProcessor<C, E> extends SingletonVerticle {

    private static final Logger logger = LoggerFactory.getLogger(AbstractProcessor.class);
    private static final int MAX_RETRIES = 3;
    private static final long DEFAULT_PARTITION_NUMBER = 0L;
    private static final long DEFAULT_SEQUENCE_NUMBER = 0L;

    private final EventStore<E> store;
    private final AtomicLong partitionNumber;
    private final AtomicLong sequenceNumber;

    public AbstractProcessor(final EventStore<E> store) {

        this.store = store;
        this.partitionNumber = new AtomicLong(DEFAULT_PARTITION_NUMBER);
        this.sequenceNumber = new AtomicLong(DEFAULT_SEQUENCE_NUMBER);
    }

    public String getPersistence() {

        return store.getPersistence();
    }

    public abstract String getPersistenceId();

    private long getPartitionNumber() {

        return partitionNumber.get();
    }

    private long getSequenceNumber() {

        return sequenceNumber.get();
    }

    @Override
    public final void start() {

        recover();
        vertx.eventBus().consumer(getAddress(), message -> {
            final Sender sender = new Sender(message);
            receive(sender, (C) sender.body());
        });
    }

    @Override
    public final void stop() {

    }

    private long calculatePartitionNumber() {
        long partitionNumber = store.getPartitionNumber(getPersistenceId(), PersistenceType.EVENT);
        return partitionNumber == -1L ? 0L : partitionNumber;
    }

    private long calculateSequenceNumber() {
        return store.getSequenceNumber(getPersistenceId(), PersistenceType.EVENT, getPartitionNumber()) + 1;
    }

    private void recover() {
        logger.info("Recovery started: " + System.currentTimeMillis());
        recoverStarted();
        partitionNumber.set(calculatePartitionNumber());
        sequenceNumber.set(calculateSequenceNumber());
        recover(1);
        recoverCompleted();
        logger.info("Recovery completed: " + System.currentTimeMillis());
    }

    private void recover(int retries) {

        logger.info("Recovery attempt: " + retries + "/" + MAX_RETRIES);
        final List<Envelope<E>> envelopes = store.getEnvelopes(getPersistenceId(), PersistenceType.EVENT,
                getPartitionNumber(), 0L);
        envelopes.forEach(envelope -> {
            logger.info("Replying event (" + envelope.getPartitionNumber() + ", " + envelope.getSequenceNumber() + "): " +
                    envelope.getValue());
            reply(envelope);
        });

        /*
        final Future<Void> recoverFuture = Future.future();
        final Optional<Envelope<E>> maybeSnapshot = store.getEvent(getPersistenceId(), PersistenceType.STATE, getPartitionNumber(), 0L);
        if (maybeSnapshot.isPresent()) {
            final Envelope<E> snapshotEnvelope = maybeSnapshot.get();
            final Optional<Envelope<E>> maybeEvent = store.getEvent(PersistenceType.EVENT, getPersistenceId(), getPartitionNumber(), 0L);
            if (!maybeEvent.isPresent()) {
                final E snapshot = snapshotEnvelope.getPayload();
                final Envelope envelope = new Envelope(UUID.randomUUID().toString(), "event", getPartitionNumber(), 0L,
                        snapshot.getClass().getName(), snapshot, System.currentTimeMillis());
                store.writeEvent(getPersistenceId(), envelope, eventEnvelope -> {
                    recoverFuture.complete();
                }, 1);
            } else {
                recoverFuture.complete();
            }
        } else {
            recoverFuture.complete();
        }
        if (recoverFuture.succeeded()) {
            final List<Envelope<E>> envelopes = store.getEvents(getPersistenceId(), PersistenceType.EVENT, getPartitionNumber(), 0L);
            envelopes.forEach(envelope -> {
                logger.info("Replying event (" + envelope.getPartitionNumber() + ", " + envelope.getSequenceNumber() + "): " +
                        envelope.getPayload());
                reply(envelope);
            });
        } else {
            if (retries >= MAX_RETRIES) {
                logger.error(recoverFuture.cause().getMessage());
                throw new RuntimeException(recoverFuture.cause());
            } else {
                logger.warn(recoverFuture.cause().getMessage());
                recover(retries + 1);
            }
        }*/
    }

    protected void recoverStarted() {

    }

    protected void recoverCompleted() {

    }

    protected void reply(final Envelope<E> envelope) {

    }

    protected abstract void receive(final Sender sender, final C command);

    protected final void persist(final E event, final BiHandler<Transaction<E>, Envelope<E>> handler) {

        final long parNum = getPartitionNumber();
        final long seqNum = sequenceNumber.getAndIncrement();
        logger.info("Persisting event (" + parNum + "," + seqNum + "): " + event);
        final Envelope envelope = new Envelope(UUID.randomUUID().toString(), PersistenceType.EVENT, parNum, seqNum,
                event.getClass().getName(), event, System.currentTimeMillis());
        final Transaction<E> transaction = new Transaction<>(store, getPersistenceId(), parNum, seqNum, envelope);
        handler.handle(transaction, envelope);
    }

    protected final void persist(final List<E> events, final BiHandler<Transaction<E>, List<Envelope<E>>> handler) {

        final List<Envelope<E>> envelopes = new ArrayList<>();
        events.forEach(event -> {
            final long parNum = getPartitionNumber();
            final long seqNum = sequenceNumber.getAndIncrement();
            logger.info("Persisting event (" + parNum + "," + seqNum + "): " + event);
            final Envelope<E> envelope = new Envelope(UUID.randomUUID().toString(), PersistenceType.EVENT, parNum, seqNum,
                    event, System.currentTimeMillis());
            envelopes.add(envelope);
        });
        final Envelope<E> envelope = envelopes.get(envelopes.size() - 1);
        final Transaction<E> transaction = new Transaction(store, getPersistenceId(), envelope.getPartitionNumber(),
                envelope.getSequenceNumber(), envelopes);
        handler.handle(transaction, envelopes);
    }

    protected final void snapshot(final E snapshot, final BiHandler<Transaction<E>, Envelope<E>> handler) {

        sequenceNumber.set(DEFAULT_SEQUENCE_NUMBER);
        final long parNum = partitionNumber.incrementAndGet();
        final long seqNum = sequenceNumber.getAndIncrement();
        logger.info("Persisting snapshot (" + parNum + "," + seqNum + "): " + snapshot);

        final Envelope eventEnvelope = new Envelope(UUID.randomUUID().toString(), PersistenceType.EVENT, parNum, seqNum,
                snapshot.getClass().getName(), snapshot, System.currentTimeMillis());
        final Envelope snapshotEnvelope = new Envelope(UUID.randomUUID().toString(), PersistenceType.SNAPSHOT, parNum, seqNum,
                snapshot.getClass().getName(), snapshot, System.currentTimeMillis());
        final List<Envelope<E>> envelopes = new ArrayList<>();
        envelopes.add(eventEnvelope);
        envelopes.add(snapshotEnvelope);
        final Transaction<E> transaction = new Transaction(store, getPersistenceId(), parNum,
                seqNum, envelopes);
        handler.handle(transaction, snapshotEnvelope);
    }

    protected void onError(final Throwable throwable) {

        logger.error(throwable.getMessage());
    }
}
