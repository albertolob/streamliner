package io.streamliner.eventsourcing.persistence;

import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.vertx.core.Handler;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Transaction<T> {

    private final EventStore<T> store;
    private String persistenceId;
    private List<Envelope<T>> envelopes;
    private long partitionNumber;
    private long sequenceNumber;

    public Transaction(final EventStore<T> store, final String persistenceId, final long partitionNumber,
                       final long sequenceNumber, final Envelope<T> envelope) {

        this.store = store;
        this.persistenceId = persistenceId;
        this.partitionNumber = partitionNumber;
        this.sequenceNumber = sequenceNumber;
        this.envelopes = new ArrayList<>();
        envelopes.add(envelope);
    }

    public Transaction(final EventStore<T> store, final String persistenceId, final long partitionNumber,
                       final long sequenceNumber, final List<Envelope<T>> envelopes) {

        this.store = store;
        this.persistenceId = persistenceId;
        this.partitionNumber = partitionNumber;
        this.sequenceNumber = sequenceNumber;
        this.envelopes = envelopes;
    }

    public void commit(Handler<Void> handler) {

        store.writeEnvelopes(persistenceId, envelopes, response -> {
            handler.handle(null);
        },1);
    }

    public void commit(final String key, final T state, final Handler<Envelope<T>> handler) {

        final Envelope envelope = new Envelope(UUID.randomUUID().toString(), PersistenceType.STATE,
                partitionNumber, sequenceNumber, key, state, System.currentTimeMillis());
        envelopes.add(envelope);
        store.writeEnvelopes(persistenceId, envelopes, response -> {
            handler.handle(response.get(response.size() - 1));
        },1);
    }
}

