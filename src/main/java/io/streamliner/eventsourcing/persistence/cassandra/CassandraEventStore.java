package io.streamliner.eventsourcing.persistence.cassandra;

import com.datastax.driver.core.*;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.messaging.protocol.Offset;
import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.serialization.Serdes;
import io.vertx.core.Handler;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class CassandraEventStore<T> extends EventStore<T> {

    private static final Logger logger = LoggerFactory.getLogger(CassandraEventStore.class);
    private static final int MAX_RETRIES = 3;
    private final Session session;
    private ExecutorService executor;

    public CassandraEventStore(final Session session, final Serdes<T> serdes, final String persistence) {
        super(serdes, persistence);
        this.session = session;
        executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        init();
    }

    private void init() {

        logger.info("Cassandra store initialization started: " + System.currentTimeMillis());

        /*
        final String statement1 = "CREATE TABLE IF NOT EXISTS " + getPersistence() + "_snapshot" +
                " (persistence_id VARCHAR," +
                " persistence_type VARCHAR," +
                " partition_number BIGINT," +
                " sequence_number BIGINT," +
                " uid VARCHAR," +
                " manifest VARCHAR," +
                " payload BLOB," +
                " timestamp BIGINT," +
                " PRIMARY KEY (persistence_id, persistence_type, partition_number, sequence_number));";
        if (logger.isDebugEnabled()) logger.info(statement1);
        session.execute(statement1); */

        final String statement2 = "CREATE TABLE IF NOT EXISTS " + getPersistence() + "_journal" +
                " (persistence_id VARCHAR," +
                " persistence_type VARCHAR," +
                " partition_number BIGINT," +
                " sequence_number BIGINT," +
                " key VARCHAR," +
                " value BLOB," +
                " manifest VARCHAR," +
                " uid VARCHAR," +
                " timestamp BIGINT," +
                " PRIMARY KEY ((persistence_id, persistence_type, partition_number), sequence_number));";
        if (logger.isDebugEnabled()) logger.info(statement2);
        session.execute(statement2);

        final String statement3 = "CREATE MATERIALIZED VIEW IF NOT EXISTS vw_" + getPersistence() + "_journal AS" +
                " SELECT persistence_id, persistence_type, partition_number, sequence_number FROM my_persistence_journal" +
                " WHERE persistence_id IS NOT NULL AND persistence_type IS NOT NULL AND partition_number IS NOT NULL AND sequence_number IS NOT NULL" +
                " PRIMARY KEY ((persistence_id, persistence_type), partition_number, sequence_number)" +
                " WITH CLUSTERING ORDER BY (partition_number DESC, sequence_number DESC);";
        if (logger.isDebugEnabled()) logger.info(statement3);
        session.execute(statement3);

        final String statement4 = "CREATE TABLE IF NOT EXISTS " + getPersistence() + "_cursor" +
                " (persistence_id VARCHAR," +
                " persistence_type VARCHAR," +
                " client_id VARCHAR," +
                " partition_number BIGINT," +
                " sequence_number BIGINT," +
                " timestamp BIGINT," +
                " PRIMARY KEY ((persistence_id, persistence_type, client_id)));";
        if (logger.isDebugEnabled()) logger.info(statement4);
        session.execute(statement4);

        logger.info("Cassandra store initialization completed: " + System.currentTimeMillis());
    }

    @Override
    public long getPartitionNumber(final String persistenceId, final String persistenceType) {

        final String query = "SELECT partition_number FROM vw_" + getPersistence() + "_journal" +
                " WHERE persistence_id = ?" +
                " AND persistence_type = ?" +
                " AND partition_number >= 0" +
                " ORDER BY partition_number DESC," +
                " sequence_number DESC" +
                " LIMIT 1";
        if (logger.isDebugEnabled()) logger.debug(query);
        final PreparedStatement preparedStatement = session.prepare(query);
        final BoundStatement boundStatement = preparedStatement.bind(persistenceId, persistenceType);
        final ResultSet resultSet = session.execute(boundStatement);
        final List<Row> rows = resultSet.all();
        return rows.size() > 0 ? rows.get(0).getLong(0) : -1L;
    }

    @Override
    public long getSequenceNumber(final String persistenceId, final String persistenceType, final long partitionNumber) {

        final String query = "SELECT sequence_number FROM vw_" + getPersistence() + "_journal" +
                " WHERE persistence_id = ?" +
                " AND persistence_type = ?" +
                " AND partition_number = ?" +
                " AND sequence_number >= 0" +
                " ORDER BY partition_number DESC," +
                " sequence_number DESC" +
                " LIMIT 1";
        if (logger.isDebugEnabled()) logger.info(query);
        final PreparedStatement preparedStatement = session.prepare(query);
        final BoundStatement boundStatement = preparedStatement.bind(persistenceId, persistenceType, partitionNumber);
        final ResultSet resultSet = session.execute(boundStatement);
        final List<Row> rows = resultSet.all();
        return rows.size() > 0 ? rows.get(0).getLong(0) : -1L;
    }

    @Override
    public Optional<Envelope<T>> getEnvelope(final String persistenceId, final String persistenceType, final long partitionNumber, final long sequenceNumber) {

        final String query = "SELECT uid, persistence_type, partition_number, sequence_number, key, value, manifest, timestamp FROM " + getPersistence() + "_journal" +
                " WHERE persistence_id = ?" +
                " AND persistence_type = ?" +
                " AND partition_number = ?" +
                " AND sequence_number = ?";
        if (logger.isDebugEnabled()) logger.info(query);
        final PreparedStatement preparedStatement = session.prepare(query);
        final BoundStatement boundStatement = preparedStatement.bind(persistenceId, persistenceType, partitionNumber, sequenceNumber);
        final ResultSet resultSet = session.execute(boundStatement);
        final List<Row> rows = resultSet.all();
        return rows.size() > 0 ? Optional.of(toEnvelope(rows.get(0))) : Optional.empty();
    }

    @Override
    public List<Envelope<T>> getEnvelopes(final String persistenceId, final String persistenceType, final long partitionNumber, final long sequenceNumber) {

        final String query = "SELECT uid, persistence_type, partition_number, sequence_number, key, value, manifest, timestamp FROM " + getPersistence() + "_journal" +
                " WHERE persistence_id = ?" +
                " AND persistence_type = ?" +
                " AND partition_number = ?" +
                " AND sequence_number >= ?";
        if (logger.isDebugEnabled()) logger.info(query);
        final PreparedStatement preparedStatement = session.prepare(query);
        final BoundStatement boundStatement = preparedStatement.bind(persistenceId, persistenceType, partitionNumber, sequenceNumber);
        final ResultSet resultSet = session.execute(boundStatement);
        final List<Row> rows = resultSet.all();
        return rows.stream().map(row -> toEnvelope(row)).collect(Collectors.toList());
    }

    @Override
    public void writeEnvelope(final String persistenceId, final Envelope envelope, final Handler<Envelope<T>> handler, final int retries) {

        final String query = "INSERT INTO " + getPersistence() + "_journal" +
                " (persistence_id, persistence_type, partition_number, sequence_number, key, value, manifest, uid, timestamp)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        if (logger.isDebugEnabled()) logger.info(query);
        final PreparedStatement preparedStatement = session.prepare(query);
        final BoundStatement boundStatement = preparedStatement.bind(persistenceId,
                envelope.getPersistenceType(),
                envelope.getPartitionNumber(),
                envelope.getSequenceNumber(),
                envelope.getKey(),
                ByteBuffer.wrap(getSerdes().serialize((T) envelope.getValue())),
                envelope.getManifest(),
                envelope.getId(),
                envelope.getTimestamp());
        final ResultSetFuture resultSetFuture = session.executeAsync(boundStatement);
        Futures.addCallback(resultSetFuture, new FutureCallback<ResultSet>() {

            @Override
            public void onSuccess(final ResultSet result) {
                handler.handle(envelope);
            }

            @Override
            public void onFailure(Throwable t) {
                if (retries >= MAX_RETRIES) {
                    throw new RuntimeException(t);
                } else {
                    writeEnvelope(persistenceId, envelope, handler, retries + 1);
                }
            }
        }, executor);
    }

    @Override
    public void writeEnvelopes(final String persistenceId, final List<Envelope<T>> envelopes, final Handler<List<Envelope<T>>> handler, final int retries) {

        final String query = "INSERT INTO " + getPersistence() + "_journal" +
                " (persistence_id, persistence_type, partition_number, sequence_number, key, value, manifest, uid, timestamp)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        if (logger.isDebugEnabled()) logger.info(query);
        final PreparedStatement preparedStatement = session.prepare(query);
        final BatchStatement batchStatement = new BatchStatement();
        envelopes.forEach(envelope -> {
            final BoundStatement boundStatement = preparedStatement.bind(persistenceId,
                    envelope.getPersistenceType(),
                    envelope.getPartitionNumber(),
                    envelope.getSequenceNumber(),
                    envelope.getKey(),
                    ByteBuffer.wrap(getSerdes().serialize(envelope.getValue())),
                    envelope.getManifest(),
                    envelope.getId(),
                    envelope.getTimestamp());
            batchStatement.add(boundStatement);
        });

        final ResultSetFuture resultSetFuture = session.executeAsync(batchStatement);
        Futures.addCallback(resultSetFuture, new FutureCallback<ResultSet>() {

            @Override
            public void onSuccess(final ResultSet result) {
                handler.handle(envelopes);
            }

            @Override
            public void onFailure(Throwable t) {
                if (retries >= MAX_RETRIES) {
                    throw new RuntimeException(t);
                } else {
                    writeEnvelopes(persistenceId, envelopes, handler, retries + 1);
                }
            }
        }, executor);
    }

    @Override
    public Optional<Offset> getOffset(final String persistenceId, final String persistenceType, final String clientId) {

        final String query = "SELECT partition_number, sequence_number FROM " + getPersistence() + "_cursor" +
                " WHERE persistence_id = ?" +
                " AND persistence_type = ?" +
                " AND client_id = ?" +
                " LIMIT 1";
        if (logger.isDebugEnabled()) logger.info(query);
        final PreparedStatement preparedStatement = session.prepare(query);
        final BoundStatement boundStatement = preparedStatement.bind(persistenceId, persistenceType, clientId);
        final ResultSet resultSet = session.execute(boundStatement);
        final List<Row> rows = resultSet.all();
        return rows.size() > 0 ? Optional.of(toOffset(rows.get(0))) : Optional.empty();
    }

    @Override
    public void acknowledge(final String persistenceId, final String persistenceType, final String clientId,
                            final long partitionNumber, final long sequenceNumber, final Handler<Offset> handler) {
        writeOffset(persistenceId, persistenceType, clientId, partitionNumber, sequenceNumber, handler, 1);
    }

    private void writeOffset(final String persistenceId, final String persistenceType, final String clientId,
                             final long partitionNumber, final long sequenceNumber, final Handler<Offset> handler, final int retries) {
        final String query = "INSERT INTO " + getPersistence() + "_cursor" +
                " (persistence_id, persistence_type, client_id, partition_number, sequence_number, timestamp)" +
                " VALUES (?, ?, ?, ?, ?, ?)";
        if (logger.isDebugEnabled()) logger.info(query);
        final PreparedStatement preparedStatement = session.prepare(query);
        final BoundStatement boundStatement = preparedStatement.bind(persistenceId,
                persistenceType,
                clientId,
                partitionNumber,
                sequenceNumber,
                System.currentTimeMillis());
        final ResultSetFuture resultSetFuture = session.executeAsync(boundStatement);
        Futures.addCallback(resultSetFuture, new FutureCallback<ResultSet>() {

            @Override
            public void onSuccess(final ResultSet result) {
                handler.handle(new Offset(partitionNumber, sequenceNumber));
            }

            @Override
            public void onFailure(Throwable t) {
                if (retries >= MAX_RETRIES) {
                    throw new RuntimeException(t);
                } else {
                    writeOffset(persistenceId, persistenceType, clientId, partitionNumber,
                            sequenceNumber, handler, retries + 1);
                }
            }
        }, executor);
    }

    private Envelope<T> toEnvelope(final Row row) {
        //uid, persistence_type, partition_number, sequence_number, key, value, manifest, timestamp
        return new Envelope(
                row.getString(0), // uid
                row.getString(1), // persistenceType
                row.getLong(2), // partitionNumber
                row.getLong(3), // sequenceNumber
                row.getString(4), // key
                getSerdes().deserialize(row.getBytes(5).array()), // value
                row.getLong(7)); // timestamp
    }

    private Offset toOffset(final Row row) {
        return new Offset(
                row.getLong(0),
                row.getLong(1));
    }
}
