package io.streamliner.eventsourcing.persistence.cassandra;


import java.io.*;

public class ObjectCodec {

    public static byte[] serialize(Object value) {

        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            final ObjectOutput output = new ObjectOutputStream(bos);
            output.writeObject(value);
            output.flush();
            return bos.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }

    public static Object deserialize(byte[] value) {

        final ByteArrayInputStream bis = new ByteArrayInputStream(value);
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            return in.readObject();
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex.getMessage());
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }

}
