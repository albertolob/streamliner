package io.streamliner.eventsourcing.persistence;

public interface BiHandler<A, B> {

    void handle(A a, B b);
}
