package io.streamliner.eventsourcing.persistence;

public final class PersistenceType {

    public static final String EVENT = "event";
    public static final String SNAPSHOT = "snapshot";
    public static final String STATE = "state";

}
