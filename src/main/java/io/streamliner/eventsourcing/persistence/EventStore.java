package io.streamliner.eventsourcing.persistence;

import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.messaging.protocol.Offset;
import io.streamliner.eventsourcing.serialization.Serdes;
import io.vertx.core.Handler;

import java.util.List;
import java.util.Optional;

public abstract class EventStore<T> {

    private final String persistence;
    private final Serdes<T> serdes;

    public EventStore(final Serdes<T> serdes, final String persistence) {
        this.serdes = serdes;
        this.persistence = persistence;
    }

    public Serdes<T> getSerdes() {

        return serdes;
    }

    public String getPersistence() {

        return persistence;
    }

    public abstract long getPartitionNumber(String persistenceId, String persistenceType);

    public abstract long getSequenceNumber(String persistenceId, String persistenceType, long partitionNumber);

    public abstract Optional<Envelope<T>> getEnvelope(String persistenceId, String persistenceType, long partitionNumber,
                                                      long sequenceNumber);

    public abstract List<Envelope<T>> getEnvelopes(String persistenceId, String persistenceType, long partitionNumber,
                                                   long sequenceNumber);

    public abstract void writeEnvelope(String persistenceId, Envelope envelope, Handler<Envelope<T>> handler,
                                    int retries);

    public abstract void writeEnvelopes(String persistenceId, List<Envelope<T>> envelopes, Handler<List<Envelope<T>>> handler,
                                     int retries);

    public abstract Optional<Offset> getOffset(String persistenceId, String persistenceType, String clientId);

    public abstract void acknowledge(String persistenceId, String persistenceType, String clientId, long partitionNumber,
                                     long sequenceNumber, Handler<Offset> handler);

}
