package io.streamliner.eventsourcing;

import io.vertx.core.*;
import io.vertx.core.impl.ConcurrentHashSet;
import io.vertx.servicediscovery.Record;
import io.vertx.servicediscovery.ServiceDiscovery;
import io.vertx.servicediscovery.ServiceDiscoveryOptions;
import io.vertx.servicediscovery.types.EventBusService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ServiceVerticle extends AbstractVerticle {

    private ServiceDiscovery discovery;
    private Set<Record> records = new ConcurrentHashSet<>();

    @Override
    public void start() {

        this.discovery = ServiceDiscovery.create(vertx, new ServiceDiscoveryOptions().setBackendConfiguration(config()));
    }

    @Override
    public void stop(final Future<Void> future) {

        final List<Future> futures = new ArrayList<>();
        records.stream().forEach(record -> {
            final Future<Void> f = Future.future();
            futures.add(f);
            discovery.unpublish(record.getRegistration(), f);
        });

        if (futures.isEmpty()) {
            discovery.close();
            future.complete();
        } else {
            final CompositeFuture composite = CompositeFuture.all(futures);
            composite.setHandler(response -> {
                discovery.close();
                if (response.failed()) {
                    future.fail(response.cause());
                } else {
                    future.complete();
                }
            });
        }
    }

    public void register(final String name, final String address, final Class service, final Handler<AsyncResult<Void>> handler) {

        final Record record = EventBusService.createRecord(name, address, service);
        register(record, handler);
    }

    protected void register(final Record record, final Handler<AsyncResult<Void>> handler) {

        if (discovery == null) {
            try {
                start();
            } catch (Exception e) {
                throw new RuntimeException("Cannot create discovery service");
            }
        }

        discovery.publish(record, response -> {
            if (response.succeeded()) {
                records.add(record);
            }
            handler.handle(response.map((Void) null));
        });
    }

}
