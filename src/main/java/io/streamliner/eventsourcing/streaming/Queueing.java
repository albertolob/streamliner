package io.streamliner.eventsourcing.streaming;

import io.streamliner.eventsourcing.messaging.protocol.Envelope;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Queueing<T> implements Consumer<Envelope<T>>, Supplier<Envelope<T>> {

    private final int length;
    private final BlockingQueue<Envelope<T>> q;

    public Queueing(int length) {
        this.length = length;
        q = new LinkedBlockingQueue<Envelope<T>>(this.length);
    }

    @Override
    public Envelope<T> get() {
        try {
            return q.take();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void accept(Envelope<T> t) {
        try {
            q.put(t);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public int maxLength() {
        return this.length;
    }

    public int size() {
        return q.size();
    }
}