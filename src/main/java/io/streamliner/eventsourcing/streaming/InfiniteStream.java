package io.streamliner.eventsourcing.streaming;

import io.streamliner.eventsourcing.messaging.protocol.Envelope;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;
import java.util.stream.Collector;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

// Consumer<T>,
public class InfiniteStream<T> implements Stream<Envelope<T>> {

    private final Stream<Envelope<T>> stream;

    public InfiniteStream(final Queueing queueing) {

        this.stream = Stream.generate(queueing);
    }

    /*
    @Override
    public void accept(T t) {
        q.accept(t);
    }
    */

    @Override
    public Iterator<Envelope<T>> iterator() {
        return stream.iterator();
    }

    @Override
    public Spliterator<Envelope<T>> spliterator() {
        return stream.spliterator();
    }

    @Override
    public boolean isParallel() {
        return stream.isParallel();
    }

    @Override
    public Stream<Envelope<T>> sequential() {
        return stream.sequential();
    }

    @Override
    public Stream<Envelope<T>> parallel() {
        return stream.parallel();
    }

    @Override
    public Stream<Envelope<T>> unordered() {
        return stream.unordered();
    }

    @Override
    public Stream<Envelope<T>> onClose(Runnable closeHandler) {
        return stream.onClose(closeHandler);
    }

    @Override
    public void close() {
        stream.close();
    }

    @Override
    public Stream<Envelope<T>> filter(Predicate<? super Envelope<T>> predicate) {
        return stream.filter(predicate);
    }

    @Override
    public <R> Stream<R> map(Function<? super Envelope<T>, ? extends R> mapper) {
        return stream.map(mapper);
    }

    @Override
    public IntStream mapToInt(ToIntFunction<? super Envelope<T>> mapper) {
        return stream.mapToInt(mapper);
    }

    @Override
    public LongStream mapToLong(ToLongFunction<? super Envelope<T>> mapper) {
        return stream.mapToLong(mapper);
    }

    @Override
    public DoubleStream mapToDouble(ToDoubleFunction<? super Envelope<T>> mapper) {
        return stream.mapToDouble(mapper);
    }

    @Override
    public <R> Stream<R> flatMap(
            Function<? super Envelope<T>, ? extends Stream<? extends R>> mapper) {
        return stream.flatMap(mapper);
    }

    @Override
    public IntStream flatMapToInt(
            Function<? super Envelope<T>, ? extends IntStream> mapper) {
        return stream.flatMapToInt(mapper);
    }

    @Override
    public LongStream flatMapToLong(
            Function<? super Envelope<T>, ? extends LongStream> mapper) {
        return stream.flatMapToLong(mapper);
    }

    @Override
    public DoubleStream flatMapToDouble(
            Function<? super Envelope<T>, ? extends DoubleStream> mapper) {
        return stream.flatMapToDouble(mapper);
    }

    @Override
    public Stream<Envelope<T>> distinct() {
        return stream.distinct();
    }

    @Override
    public Stream<Envelope<T>> sorted() {
        return stream.sorted();
    }

    @Override
    public Stream<Envelope<T>> sorted(Comparator<? super Envelope<T>> comparator) {
        return stream.sorted(comparator);
    }

    @Override
    public Stream<Envelope<T>> peek(Consumer<? super Envelope<T>> action) {
        return stream.peek(action);
    }

    @Override
    public Stream<Envelope<T>> limit(long maxSize) {
        return stream.limit(maxSize);
    }

    @Override
    public Stream<Envelope<T>> skip(long n) {
        return stream.skip(n);
    }

    @Override
    public void forEach(Consumer<? super Envelope<T>> action) {
        stream.forEach(action);
    }

    @Override
    public void forEachOrdered(Consumer<? super Envelope<T>> action) {
        stream.forEachOrdered(action);
    }

    @Override
    public Object[] toArray() {
        return stream.toArray();
    }

    @Override
    public <A> A[] toArray(IntFunction<A[]> generator) {
        return stream.toArray(generator);
    }

    @Override
    public Envelope<T> reduce(Envelope<T> identity, BinaryOperator<Envelope<T>> accumulator) {
        return stream.reduce(identity, accumulator);
    }

    @Override
    public Optional<Envelope<T>> reduce(BinaryOperator<Envelope<T>> accumulator) {
        return stream.reduce(accumulator);
    }

    @Override
    public <U> U reduce(U identity, BiFunction<U, ? super Envelope<T>, U> accumulator,
                        BinaryOperator<U> combiner) {
        return stream.reduce(identity, accumulator, combiner);
    }

    @Override
    public <R> R collect(Supplier<R> supplier,
                         BiConsumer<R, ? super Envelope<T>> accumulator, BiConsumer<R, R> combiner) {
        return stream.collect(supplier, accumulator, combiner);
    }

    @Override
    public <R, A> R collect(Collector<? super Envelope<T>, A, R> collector) {
        return stream.collect(collector);
    }

    @Override
    public Optional<Envelope<T>> min(Comparator<? super Envelope<T>> comparator) {
        return stream.min(comparator);
    }

    @Override
    public Optional<Envelope<T>> max(Comparator<? super Envelope<T>> comparator) {
        return stream.max(comparator);
    }

    @Override
    public long count() {
        return stream.count();
    }

    @Override
    public boolean anyMatch(Predicate<? super Envelope<T>> predicate) {
        return stream.anyMatch(predicate);
    }

    @Override
    public boolean allMatch(Predicate<? super Envelope<T>> predicate) {
        return stream.allMatch(predicate);
    }

    @Override
    public boolean noneMatch(Predicate<? super Envelope<T>> predicate) {
        return stream.noneMatch(predicate);
    }

    @Override
    public Optional<Envelope<T>> findFirst() {
        return stream.findFirst();
    }

    @Override
    public Optional<Envelope<T>> findAny() {
        return stream.findAny();
    }
}