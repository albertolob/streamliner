package io.streamliner.eventsourcing.streaming;

import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.messaging.protocol.Offset;

import java.util.List;
import java.util.Optional;

public class ConsumerTask<T> implements Runnable {

    private final Queueing queueing;
    private final EventStore<T> store;
    private final String persistenceId;
    private final String persistenceType;
    private final String clientId;
    private Optional<Offset> offset;
    private boolean isRunning;

    public ConsumerTask(final EventStore<T> store, final String persistenceId, final String persistenceType, final String clientId) {
        this.queueing = new Queueing(1000);
        this.store = store;
        this.persistenceId = persistenceId;
        this.persistenceType = persistenceType;
        this.clientId = clientId;
        this.offset = store.getOffset(getPersistenceId(), getPersistenceType(), getClientId());
    }

    public Queueing getQueueing() {

        return queueing;
    }

    public String getPersistenceId() {

        return persistenceId;
    }

    public String getPersistenceType() {
        return persistenceType;
    }

    public String getClientId() {

        return clientId;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void start() {
        if (!isRunning()) {
            isRunning = true;
            new Thread(this).start();
        } else {
            throw new RuntimeException("ConsumerTask is already running");
        }
    }

    public void stop() {
        if (isRunning()) {
            isRunning = false;
        } else {
            throw new RuntimeException("ConsumerTask is not running");
        }
    }

    public void run() {
        while (isRunning) {
            if (queueing.size() < queueing.maxLength()) {
                long partitionNumber = offset.isPresent() ? offset.get().getPartitionNumber() : 0;
                long sequenceNumber = offset.isPresent() ? offset.get().getSequenceNumber() + 1 : 0;
                System.out.println(getPersistenceId() + " " + partitionNumber + " " + sequenceNumber);
                List<Envelope<T>> envelopes = store.getEnvelopes(getPersistenceId(), getPersistenceType(), partitionNumber, sequenceNumber);
                if (envelopes.size() == 0) {
                    envelopes = store.getEnvelopes(getPersistenceId(), getPersistenceType(), partitionNumber + 1, 0L);
                    envelopes.forEach(envelope -> {
                        queueing.accept(envelope);
                    });
                } else {
                    envelopes.forEach(envelope -> {
                        queueing.accept(envelope);
                    });
                }
                if (envelopes.size() > 0) {
                    final Envelope<T> envelope = envelopes.get(envelopes.size() - 1);
                    offset = Optional.of(new Offset(envelope.getPartitionNumber(),
                            envelope.getSequenceNumber()));
                } else {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {

                    }
                }
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {

            }
        }

    }
}
