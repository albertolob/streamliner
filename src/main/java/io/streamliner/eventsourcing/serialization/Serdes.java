package io.streamliner.eventsourcing.serialization;

public interface Serdes<T> extends Serializer<T>, Deserializer<T> {

}
