package io.streamliner.eventsourcing.serialization;

public interface Deserializer<T> {

    T deserialize(byte[] value);
}
