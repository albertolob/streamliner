package io.streamliner.eventsourcing.serialization;

public interface Serializer<T> {

    byte[] serialize(T value);
}
