package io.streamliner.eventsourcing.messaging.protocol;

import java.io.Serializable;

public final class Offset implements Serializable, Comparable<Offset> {

    private final long partitionNumber;
    private final long sequenceNumber;

    public Offset(final long partitionNumber, final long sequenceNumber) {

        this.partitionNumber = partitionNumber;
        this.sequenceNumber = sequenceNumber;
    }

    public static Offset fromString(String value) {

        final String[] tokens = value.split("\\:");
        return new Offset(Long.parseLong(tokens[0]), Long.parseLong(tokens[1]));
    }

    public long getPartitionNumber() {

        return partitionNumber;
    }

    public long getSequenceNumber() {

        return sequenceNumber;
    }

    public int compareTo(final Offset sequence) {
        if (sequence == null) {
            return 1;
        } else {
            if (getPartitionNumber() > sequence.getPartitionNumber()) {
                return 1;
            } else if (getPartitionNumber() == sequence.getPartitionNumber()) {
                if (getSequenceNumber() > sequence.getSequenceNumber()) {
                    return 1;
                } else if (getSequenceNumber() < sequence.getSequenceNumber()) {
                    return -1;
                } else {
                    return 0;
                }
            } else {
                return -1;
            }
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(getPartitionNumber());
        builder.append(':');
        builder.append(getSequenceNumber());
        return builder.toString();
    }
}
