package io.streamliner.eventsourcing.messaging.protocol;

public final class Envelope<V> {

    private final String id;
    private final String persistenceType;
    private final long partitionNumber;
    private final long sequenceNumber;
    private final String key;
    private final V value;
    private final long timestamp;

    public Envelope(final String id, final String persistenceType, final long partitionNumber, final long sequenceNumber,
                    final String key, final V value, final long timestamp) {
        this.id = id;
        this.persistenceType = persistenceType;
        this.partitionNumber = partitionNumber;
        this.sequenceNumber = sequenceNumber;
        this.key = key;
        this.value = value;
        this.timestamp = timestamp;
    }

    public Envelope(final String uid, final String persistenceType, final long partitionNumber, final long sequenceNumber,
                    final V value, final long timestamp) {

        this(uid, persistenceType, partitionNumber, sequenceNumber, null, value, timestamp);
    }

    public String getId() {
        return id;
    }

    public String getPersistenceType() {
        return persistenceType;
    }

    public long getPartitionNumber() {
        return partitionNumber;
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public String getManifest() {
        return value == null ? null : value.getClass().getName();
    }

    public String getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Offset getOffset() {
        return new Offset(getPartitionNumber(), getSequenceNumber());
    }
}
