package io.streamliner.eventsourcing.messaging;

import io.streamliner.eventsourcing.persistence.cassandra.ObjectCodec;
import io.vertx.codegen.annotations.CacheReturn;
import io.vertx.codegen.annotations.Nullable;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;

public class Sender {

    private Message<Object> dispacher;

    public Sender(final Message<Object> dispacher) {

        this.dispacher = dispacher;
    }

    /**
     * The address the message was sent to
     */
    public String address() {
        return dispacher.address();
    }

    /**
     * Multi-map of message headers. Can be empty
     *
     * @return  the headers
     */
    public MultiMap headers() {

        return dispacher.headers();
    }

    /**
     * The body of the message. Can be null.
     *
     * @return  the body, or null.
     */
    @CacheReturn
    public Object body() {
        return ObjectCodec.deserialize((byte[]) dispacher.body());
    }

    /**
     * The reply address. Can be null.
     *
     * @return the reply address, or null, if message was sent without a reply handler.
     */
    @Nullable
    public String replyAddress() {
        return dispacher.replyAddress();
    }

    /**
     * Signals if this message represents a send or publish event.
     *
     * @return true if this is a send.
     */
    public boolean isSend() {
        return dispacher.isSend();
    }

    /**
     * Reply to this message.
     * <p>
     * If the message was sent specifying a reply handler, that handler will be
     * called when it has received a reply. If the message wasn't sent specifying a receipt handler
     * this method does nothing.
     *
     * @param message  the message to reply with.
     */
    public void reply(Object message) {

        this.dispacher.reply(ObjectCodec.serialize(message));
    }

    /**
     * The same as {@code reply(R message)} but you can specify handler for the reply - i.e.
     * to receive the reply to the reply.
     *
     * @param message  the message to reply with.
     * @param replyHandler  the reply handler for the reply.
     */
    public <R> void reply(Object message, Handler<AsyncResult<Message<R>>> replyHandler) {

        reply(ObjectCodec.serialize(message), replyHandler);
    }

    /**
     * Link {@link #reply(Object)} but allows you to specify delivery options for the reply.
     *
     * @param message  the reply message
     * @param options  the delivery options
     */
    public void reply(Object message, DeliveryOptions options) {

        dispacher.reply(ObjectCodec.serialize(message), options);
    }

    /**
     * The same as {@code reply(R message, DeliveryOptions)} but you can specify handler for the reply - i.e.
     * to receive the reply to the reply.
     *
     * @param message  the reply message
     * @param options  the delivery options
     * @param replyHandler  the reply handler for the reply.
     */
    public <R> void reply(Object message, DeliveryOptions options, Handler<AsyncResult<Message<R>>> replyHandler) {

        dispacher.reply(ObjectCodec.serialize(message), options, replyHandler);
    }

    /**
     * Signal to the sender that processing of this message failed.
     * <p>
     * If the message was sent specifying a result handler
     * the handler will be called with a failure corresponding to the failure code and message specified here.
     *
     * @param failureCode A failure code to pass back to the sender
     * @param message A message to pass back to the sender
     */
    public void fail(int failureCode, String message) {
        dispacher.fail(failureCode, message);
    }
}
