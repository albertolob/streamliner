package io.streamliner.eventsourcing;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.persistence.cassandra.CassandraEventStore;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.messaging.protocol.Event;
import io.streamliner.eventsourcing.processor.EventConsumer;
import io.streamliner.eventsourcing.serialization.Serdes;

import java.util.stream.Stream;

public class EventConsumerTest {

    public static void main(String[] args) {

        final Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        final Session session = cluster.connect("my_keyspace_1");
        final Serdes<Event> serdes = new EventSerdes();
        final EventStore<Event> store = new CassandraEventStore<>(session, serdes, "my_persistence");
        final EventConsumer<Event> consumer = new EventConsumer(store, "test_persistence_2", "client_1");
        final Stream<Envelope<Event>> eventStream = consumer.getStream();
        eventStream.forEach(envelope -> {
            System.out.println(envelope);
            consumer.acknowledge(envelope.getOffset(), offset -> {
                System.out.println("Acknowledged offset " + offset);
            });
        });

    }

}
