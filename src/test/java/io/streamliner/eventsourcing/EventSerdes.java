package io.streamliner.eventsourcing;

import io.streamliner.eventsourcing.messaging.protocol.Event;
import io.streamliner.eventsourcing.serialization.Serdes;

import java.io.*;

public class EventSerdes implements Serdes<Event> {

    @Override
    public byte[] serialize(final Event value) {

        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            final ObjectOutput output = new ObjectOutputStream(bos);
            output.writeObject(value);
            output.flush();
            return bos.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex.getMessage());
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }

    @Override
    public Event deserialize(final byte[] value) {

        final ByteArrayInputStream bis = new ByteArrayInputStream(value);
        ObjectInput in = null;
        try {
            in = new ObjectInputStream(bis);
            return (Event) in.readObject();
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException(ex.getMessage());
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                // ignore close exception
            }
        }
    }
}
