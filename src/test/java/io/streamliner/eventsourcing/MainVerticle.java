package io.streamliner.eventsourcing;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import io.streamliner.eventsourcing.clustering.ClusteredVerticle;
import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.persistence.cassandra.CassandraEventStore;
import io.streamliner.eventsourcing.messaging.protocol.Event;
import io.streamliner.eventsourcing.serialization.Serdes;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.spi.cluster.ClusterManager;
import io.vertx.core.spi.cluster.NodeListener;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;

import java.util.Arrays;
import java.util.List;

public class MainVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(MainVerticle.class);
    private final ClusterManager clusterManager;
    private EventStore<Event> store;
    private String persistenceId;

    public MainVerticle(final ClusterManager clusterManager, final EventStore<Event> store, final String persistenceId) {
        this.clusterManager = clusterManager;
        this.clusterManager.nodeListener(new Listener());
        this.store = store;
        this.persistenceId = persistenceId;
    }

    @Override
    public void start() throws InterruptedException {

        final VertxOptions options = new VertxOptions().setClusterManager(clusterManager);

        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                vertx = res.result();

                final List<AbstractVerticle> verticles = Arrays.asList(new ClusteredVerticle(new MQTTVerticle(store, persistenceId),
                                store.getPersistence() + "->" + persistenceId),
                        new WebVerticle(store.getPersistence(), persistenceId));

                verticles.stream().forEach(verticle -> vertx.deployVerticle(verticle, deployResponse -> {

                    if (deployResponse.failed()) {
                        logger.error("Unable to deploy verticle " + verticle.getClass().getSimpleName(),
                                deployResponse.cause());
                    } else {
                        logger.info(verticle.getClass().getSimpleName() + " deployed");
                    }
                }));


                //vertx.setPeriodic(TimeUnit.SECONDS.toMillis(5), event -> {

                //    logger.info("TIMER TRIGGERED");
                /*
                if (serviceCount.get() != verticles.size()) {
                    logger.error("Main Verticle was unable to start child verticles");
                } else {
                    logger.info("Start up successful");
                } */
                //    });

            } else {
                logger.error("Cluster Error ");
                // failed!
            }
        });

    }

    private class Listener implements NodeListener {

        @Override
        public void nodeAdded(String nodeID) {
            System.out.println("added" + nodeID);
        }

        @Override
        public void nodeLeft(String nodeID) {
            System.out.println("removed" + nodeID);
        }
    }


    public static void main(String[] args) {

        final Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
        final Session session = cluster.connect("my_keyspace_1");
        final Serdes<Event> serdes = new EventSerdes();
        final EventStore<Event> store = new CassandraEventStore<>(session, serdes, "my_persistence");
        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new MainVerticle(new HazelcastClusterManager(), store, "test_persistence_2"));
    }
}
