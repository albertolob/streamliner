package io.streamliner.eventsourcing;

import io.streamliner.eventsourcing.messaging.protocol.State;
import io.streamliner.eventsourcing.messaging.protocol.state.Status;
import io.streamliner.eventsourcing.persistence.Transaction;
import io.streamliner.eventsourcing.processor.AbstractProcessor;
import io.streamliner.eventsourcing.messaging.Sender;
import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.messaging.protocol.Command;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.messaging.protocol.Event;
import io.streamliner.eventsourcing.messaging.protocol.commands.Subscribe;
import io.streamliner.eventsourcing.messaging.protocol.commands.UnSubscribe;
import io.streamliner.eventsourcing.messaging.protocol.events.Subscribed;
import io.streamliner.eventsourcing.messaging.protocol.events.UnSubscribed;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

import java.util.*;

public class MQTTVerticle extends AbstractProcessor<Command, Event> {

    private static final Logger logger = LoggerFactory.getLogger(MQTTVerticle.class);
    private final String persistenceId;
    private final Map<String, Status> states;
    private final List<Event> events;

    public MQTTVerticle(final EventStore<Event> store, final String persistenceId) {

        super(store);
        this.persistenceId = persistenceId;
        this.states = new HashMap<>();
        this.events = new ArrayList<>();
    }

    @Override
    public String getPersistenceId() {
        return persistenceId;
    }

    @Override
    protected void recoverStarted() {

        states.clear();
    }

    @Override
    protected void reply(final Envelope<Event> envelope) {

        final Event event = envelope.getValue();
        if (event instanceof Subscribed) {
            update(update((Subscribed) event));
        } else if (event instanceof UnSubscribed) {
            update(update((UnSubscribed) event));
        }
    }

    @Override
    protected void recoverCompleted() {

    }


    private Status update(final Subscribed event) {

        events.add(event);
        return new Status(event.getUserId(), "ONLINE", System.currentTimeMillis());
    }

    private Status update(final UnSubscribed event) {

        events.add(event);
        return new Status(event.getUserId(), "OFFLINE", System.currentTimeMillis());
    }

    private void update(final Status status) {

        states.put(status.getId(), status);
    }

    @Override
    protected void receive(final Sender sender, final Command command) {

        logger.info("Received command " + command);
        if (command instanceof Subscribe) {
            process(sender, (Subscribe) command);
            if (events.size() > 3 && events.size() % 2 == 0) {
                snapshot(new UnSubscribed(((Subscribe) command).getUserId()), (transaction, snapshot) -> {
                    transaction.commit(x -> {

                    });
                    logger.info("Snapshot -> " + snapshot);
                });
            }
        } else if (command instanceof UnSubscribe) {
            process(sender, (UnSubscribe) command);
        }
    }

    private boolean isOnline(String id) {

        final Status status = states.get(id);
        return status == null ? false : status.getStatus().equals("ONLINE");
    }

    private boolean isOffline(String id) {

        final Status status = states.get(id);
        return status == null ? false : status.getStatus().equals("OFFLINE");
    }

    private void process(final Sender sender, final Subscribe command) {
        logger.info("Processing command " + command);
        final Event event = new Subscribed(command.getUserId());
        persist(event, (transaction, envelope) -> {
            final Subscribed subscribed = (Subscribed) envelope.getValue();
            final Status status = update(subscribed);
            if (isOffline(status.getId())) {
                // The status is changed. Emit change log
                transaction.commit(status.getId(), status, state -> {
                    update(status);
                    logger.info("State is " + states.size());
                    sender.reply(subscribed);
                });
            } else {
                transaction.commit(e -> {
                    update(status);
                    logger.info("State is " + states.size());
                    sender.reply(subscribed);
                });
            }
        });
    }

    private void process(final Sender sender, final UnSubscribe command) {
        logger.info("Processing command " + command);
        final Event event = new UnSubscribed(command.getUserId());
        persist(event, (transaction, envelope) -> {
            final UnSubscribed unSubscribed = (UnSubscribed) envelope.getValue();
            final Status status = update(unSubscribed);
            if (isOffline(status.getId())) {
                // The status is changed, emit change log
                transaction.commit(status.getId(), status, state -> {
                    update(status);
                    logger.info("State is " + states.size());
                    sender.reply(unSubscribed);
                });
            } else {
                transaction.commit(e -> {
                    update(status);
                    logger.info("State is " + states.size());
                    sender.reply(unSubscribed);
                });
            }
            //logger.info("State is " + events.size());
        });
    }

}
