package io.streamliner.eventsourcing.messaging.protocol.events;

import io.streamliner.eventsourcing.messaging.protocol.Event;

public class UnSubscribed implements Event {

    private final String userId;

    public UnSubscribed(final String userId) {

        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}
