package io.streamliner.eventsourcing.messaging.protocol;

import java.io.Serializable;

public interface Command extends Serializable {
}
