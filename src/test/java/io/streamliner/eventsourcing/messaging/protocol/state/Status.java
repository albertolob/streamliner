package io.streamliner.eventsourcing.messaging.protocol.state;

import io.streamliner.eventsourcing.messaging.protocol.State;

public class Status implements State {

    private final String id;
    private final String status;
    private final long timestamp;

    public Status(final String id, final String status, final long timestamp) {

        this.id = id;
        this.status = status;
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
