package io.streamliner.eventsourcing.messaging.protocol.events;

import io.streamliner.eventsourcing.messaging.protocol.Event;

public class Subscribed implements Event {

    private final String userId;

    public Subscribed(final String userId) {

        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

}
