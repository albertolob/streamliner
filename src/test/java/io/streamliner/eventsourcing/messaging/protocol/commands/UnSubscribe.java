package io.streamliner.eventsourcing.messaging.protocol.commands;

import io.streamliner.eventsourcing.messaging.protocol.Command;

public class UnSubscribe implements Command {

    private final String userId;

    public UnSubscribe(final String userId) {

        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}
