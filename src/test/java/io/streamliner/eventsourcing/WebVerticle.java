package io.streamliner.eventsourcing;

import io.streamliner.eventsourcing.persistence.cassandra.ObjectCodec;
import io.streamliner.eventsourcing.messaging.protocol.commands.Subscribe;
import io.vertx.core.*;
import io.vertx.core.eventbus.MessageProducer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class WebVerticle extends AbstractVerticle {

    private final Logger logger = LoggerFactory.getLogger(WebVerticle.class);
    private final String persistence;
    private final String persistenceId;


    public WebVerticle(final String persistence, final String persistenceId) {

        this.persistence = persistence;
        this.persistenceId = persistenceId;
    }

    /*
    @Override
    public void init(Vertx vertx, Context context) {

    } */

    @Override
    public void start() throws Exception {

        vertx.createHttpServer()
                .requestHandler(httpRequest -> handleHttpRequest(httpRequest) )
                .listen(9090);
    }

    private void handleHttpRequest(final HttpServerRequest httpRequest) {

        final String address = persistence + "->" + persistenceId;
        logger.info("Sending message to: " + address);
        final MessageProducer<byte[]> producer = vertx.eventBus().sender(address);
        producer.exceptionHandler(e -> {
            logger.error(e.getMessage());
            httpRequest.response().setStatusCode(500).end(e.getMessage());
        });
        producer.send(ObjectCodec.serialize(new Subscribe("user1")), response -> {
            if (response.succeeded()) {
                httpRequest.response().end(response.result().body().toString());
            } else {
                logger.error(response.cause().getMessage());
                httpRequest.response().setStatusCode(500).end(response.cause().getMessage());
            }
        });
    }

    @Override
    public void stop() throws Exception {

    }
}
