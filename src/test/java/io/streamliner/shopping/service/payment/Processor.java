package io.streamliner.shopping.service.payment;

import io.streamliner.eventsourcing.messaging.Sender;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.processor.AbstractProcessor;
import io.streamliner.shopping.protocol.Command;
import io.streamliner.shopping.protocol.Event;
import io.streamliner.shopping.service.orderinterface.protocol.command.ConfirmOrder;
import io.streamliner.shopping.service.orderinterface.protocol.command.RejectOrder;
import io.streamliner.shopping.service.orderinterface.protocol.command.ValidateOrder;
import io.streamliner.shopping.service.orderinterface.protocol.event.OrderConfirmed;
import io.streamliner.shopping.service.orderinterface.protocol.event.OrderRejected;
import io.streamliner.shopping.service.orderinterface.protocol.event.OrderValidated;
import io.streamliner.shopping.service.orderinterface.protocol.state.OrderState;
import io.streamliner.shopping.service.payment.protocol.command.ProcessPayment;
import io.streamliner.shopping.service.payment.protocol.event.PaymentProcessed;
import io.streamliner.shopping.service.payment.protocol.event.PaymentRejected;
import io.streamliner.shopping.service.payment.protocol.state.PaymentState;

import java.rmi.server.UID;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

public class Processor extends AbstractProcessor<Command, Event> {

    private final String persistenceId;
    private final Map<String, PaymentState> states;

    public Processor(final EventStore<Event> store, final String persistenceId) {
        super(store);
        this.persistenceId = persistenceId;
        this.states = new HashMap<>();
    }

    @Override
    public String getPersistenceId() {

        return persistenceId;
    }

    @Override
    protected void reply(final Envelope<Event> envelope) {

        final Event event = envelope.getValue();
        if (event instanceof PaymentProcessed) {
            final PaymentState state = update((PaymentProcessed) event);
            states.put(state.getId(), state);
        } else if (event instanceof PaymentRejected) {
            // Skip
        } else {

        }
    }

    @Override
    public void receive(final Sender sender, final Command command) {

        if (command instanceof ProcessPayment) {
            receive(sender, (ProcessPayment) command);
        } else {

        }
    }

    private void receive(final Sender sender, final ProcessPayment command) {

        final Random rand = new Random();

        if (rand.nextInt(10) >= 8) {
            final PaymentRejected event = new PaymentRejected();
            event.setOrderId(command.getOrderId());
            event.setReason("Random reason");
            event.setTimestamp(System.currentTimeMillis());
            persist(event, (transaction, eventEnveope) -> {
                transaction.commit(v -> {
                    sender.reply(event);
                });
            });
        } else {
            final PaymentProcessed event = new PaymentProcessed();
            event.setId(new UID().toString());
            event.setCardHolder(command.getCardHolder());
            event.setCardType(command.getCardType());
            event.setCardNumber(command.getCardNumber());
            event.setAmount(command.getAmount());
            event.setCurrency(command.getCurrency());
            event.setOrderId(command.getOrderId());
            event.setTimestamp(System.currentTimeMillis());
            persist(event, (transaction, eventEnvelope) -> {
                final PaymentState state = update(event);
                transaction.commit(state.getId(), state, stateEnvelope -> {
                    states.put(state.getId(), state);
                    sender.reply(event);
                });
            });
        }
    }

    private PaymentState update(final PaymentProcessed event) {
        final PaymentState state = new PaymentState();
        state.setId(event.getId());
        state.setCardHolder(event.getCardHolder());
        state.setCardType(event.getCardType());
        state.setCardNumber(event.getCardNumber());
        state.setAmount(event.getAmount());
        state.setCurrency(event.getCurrency());
        state.setOrderId(event.getOrderId());
        state.setStatus("PROCESSED");
        event.setTimestamp(System.currentTimeMillis());
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }
}
