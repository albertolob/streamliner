package io.streamliner.shopping.service.payment.protocol.event;

import io.streamliner.shopping.protocol.Event;

public class PaymentRejected implements Event {

    private String reason;
    private String orderId;
    private long timestamp;

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getReason() {
        return reason;
    }

    public long getTimestamp() {
        return timestamp;
    }

}
