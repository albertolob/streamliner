package io.streamliner.shopping.service.payment;

import io.streamliner.shopping.service.orderinterface.protocol.event.OrderValidated;
import io.streamliner.shopping.service.payment.protocol.command.ProcessPayment;

public class Adapter {

    public ProcessPayment receive(OrderValidated event) {

        final ProcessPayment command = new ProcessPayment();
        command.setCardHolder(event.getPaymentDetail().getCardHolder());
        command.setCardType(event.getPaymentDetail().getCardType());
        command.setCardNumber(event.getPaymentDetail().getCardNumber());
        command.setAmount(event.getPaymentDetail().getAmount());
        command.setCurrency(event.getPaymentDetail().getCurrency());
        command.setOrderId(event.getId());
        command.setTimestamp(event.getTimestamp());
        return command;
    }
}
