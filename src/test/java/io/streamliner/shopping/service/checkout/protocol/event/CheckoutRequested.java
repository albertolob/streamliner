package io.streamliner.shopping.service.checkout.protocol.event;

import io.streamliner.shopping.protocol.Event;
import io.streamliner.shopping.service.checkout.protocol.model.PaymentDetail;

public class CheckoutRequested implements Event {

    private String id;
    private String sku;
    private int quantity;
    private double price;
    private String customerId;
    private PaymentDetail paymentDetail;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setPaymentDetail(PaymentDetail paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public PaymentDetail getPaymentDetail() {
        return paymentDetail;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
