package io.streamliner.shopping.service.checkout.protocol.event;

import io.streamliner.shopping.protocol.Event;

public class CheckoutConfirmed implements Event {

    private String id;
    private String orderId;
    private String paymentId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
