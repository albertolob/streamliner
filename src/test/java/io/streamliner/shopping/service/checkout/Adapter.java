package io.streamliner.shopping.service.checkout;

import io.streamliner.shopping.protocol.Command;
import io.streamliner.shopping.service.checkout.protocol.command.ConfirmCheckout;
import io.streamliner.shopping.service.checkout.protocol.command.RejectCheckout;
import io.streamliner.shopping.service.orderinterface.protocol.event.OrderConfirmed;
import io.streamliner.shopping.service.orderinterface.protocol.event.OrderRejected;

public class Adapter {

    public Command receive(OrderConfirmed event) {

        final ConfirmCheckout command = new ConfirmCheckout();
        command.setId(event.getCheckoutId());
        command.setOrderId(event.getId());
        command.setPaymentId(event.getPaymentId());
        command.setTimestamp(event.getTimestamp());
        return command;
    }

    public Command receive(OrderRejected event) {

        final RejectCheckout command = new RejectCheckout();
        command.setId(event.getCheckoutId());
        command.setReason(event.getReason());
        command.setOrderId(event.getId());
        command.setTimestamp(event.getTimestamp());
        return command;
    }

}
