package io.streamliner.shopping.service.checkout;

import io.streamliner.eventsourcing.messaging.Sender;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.processor.AbstractProcessor;
import io.streamliner.shopping.protocol.Command;
import io.streamliner.shopping.protocol.Event;
import io.streamliner.shopping.service.checkout.protocol.command.ConfirmCheckout;
import io.streamliner.shopping.service.checkout.protocol.command.RejectCheckout;
import io.streamliner.shopping.service.checkout.protocol.command.RequestCheckout;
import io.streamliner.shopping.service.checkout.protocol.event.CheckoutConfirmed;
import io.streamliner.shopping.service.checkout.protocol.event.CheckoutRejected;
import io.streamliner.shopping.service.checkout.protocol.event.CheckoutRequested;
import io.streamliner.shopping.service.checkout.protocol.state.CheckoutState;

import java.rmi.server.UID;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Processor extends AbstractProcessor<Command, Event> {

    private final String persistenceId;
    private final Map<String, CheckoutState> checkouts;

    public Processor(final EventStore<Event> store, final String persistenceId) {
        super(store);
        this.persistenceId = persistenceId;
        this.checkouts = new HashMap<>();
    }

    @Override
    public String getPersistenceId() {

        return persistenceId;
    }

    @Override
    protected void reply(final Envelope<Event> envelope) {

        final Event event = envelope.getValue();
        if (event instanceof CheckoutRequested) {
            final CheckoutState state = update((CheckoutRequested) event);
            checkouts.put(state.getId(), state);
        } else if (event instanceof CheckoutConfirmed) {
            final CheckoutState state = update((CheckoutConfirmed) event);
            checkouts.put(state.getId(), state);
        } else if (event instanceof CheckoutRejected) {
            final CheckoutState state = update((CheckoutRejected) event);
            checkouts.remove(state.getId());
        } else {

        }
    }

    @Override
    public void receive(final Sender sender, final Command command) {

        if (command instanceof RequestCheckout) {
            receive(sender, (RequestCheckout) command);
        } else if (command instanceof ConfirmCheckout) {
            receive(sender, (ConfirmCheckout) command);
        } else if (command instanceof RejectCheckout) {
            receive(sender, (RejectCheckout) command);
        } else {

        }
    }

    private void receive(final Sender sender, final RequestCheckout command) {

        final CheckoutRequested event = new CheckoutRequested();
        event.setId(new UID().toString());
        event.setSku(command.getSku());
        event.setQuantity(command.getQuantity());
        event.setPrice(command.getPrice());
        event.setPaymentDetail(command.getPaymentDetail());
        event.setCustomerId(command.getCustomerId());
        event.setTimestamp(System.currentTimeMillis());
        persist(event, (transaction, eventEnvelope) -> {
            final CheckoutState state = update(event);
            transaction.commit(state.getId(), state, stateEnvelope -> {
                checkouts.put(state.getId(), state);
                sender.reply(event);
            });
        });
    }

    private void receive(final Sender sender, final ConfirmCheckout command) {

        final CheckoutConfirmed event = new CheckoutConfirmed();
        event.setId(command.getId());
        event.setOrderId(command.getOrderId());
        event.setPaymentId(command.getPaymentId());
        event.setTimestamp(System.currentTimeMillis());
        persist(event, (transaction, eventEnvelope) -> {
            final CheckoutState state = update(event);
            transaction.commit(state.getId(), state, stateEnvelope -> {
                checkouts.put(state.getId(), state);
                sender.reply(event);
            });
        });
    }

    private void receive(final Sender sender, final RejectCheckout command) {

        final CheckoutRejected event = new CheckoutRejected();
        event.setId(command.getId());
        event.setReason(command.getReason());
        event.setTimestamp(System.currentTimeMillis());
        persist(event, (transaction, eventEnvelope) -> {
            final CheckoutState state = update(event);
            transaction.commit(state.getId(), state, stateEnvelope -> {
                checkouts.remove(state.getId());
                sender.reply(event);
            });
        });
    }

    private CheckoutState update(final CheckoutRequested event) {
        final CheckoutState state = new CheckoutState();
        state.setId(event.getId());
        state.setSku(event.getSku());
        state.setQuantity(event.getQuantity());
        state.setPrice(event.getPrice());
        state.setPaymentDetail(event.getPaymentDetail());
        state.setCustomerId(event.getCustomerId());
        state.setStatus("REQUESTED");
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

    private CheckoutState update(final CheckoutConfirmed event) {
        final CheckoutState state = new CheckoutState();
        state.setId(event.getId());
        state.setOrderId(Optional.of(event.getOrderId()));
        state.setPaymentId(Optional.of(event.getPaymentId()));
        state.setStatus("CONFIRMED");
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

    private CheckoutState update(final CheckoutRejected event) {
        final CheckoutState state = new CheckoutState();
        state.setId(event.getId());
        state.setStatus("REJECTED");
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

}
