package io.streamliner.shopping.service.checkout.protocol.state;

import io.streamliner.shopping.protocol.State;
import io.streamliner.shopping.service.checkout.protocol.model.PaymentDetail;

import java.util.Optional;

public class CheckoutState implements State {

    private String id;
    private String sku;
    private int quantity;
    private double price;
    private String customerId;
    private PaymentDetail paymentDetail;
    private Optional<String> orderId;
    private Optional<String> paymentId;
    private String status;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setPaymentDetail(PaymentDetail paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public void setOrderId(Optional<String> orderId) {
        this.orderId = orderId;
    }

    public void setPaymentId(Optional<String> paymentId) {
        this.paymentId = paymentId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public PaymentDetail getPaymentDetail() {
        return paymentDetail;
    }

    public Optional<String> getOrderId() {
        return orderId;
    }

    public Optional<String> getPaymentId() {
        return paymentId;
    }

    public String getStatus() {
        return status;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
