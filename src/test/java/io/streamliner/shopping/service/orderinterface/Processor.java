package io.streamliner.shopping.service.orderinterface;

import io.streamliner.eventsourcing.messaging.Sender;
import io.streamliner.eventsourcing.messaging.protocol.Envelope;
import io.streamliner.eventsourcing.persistence.EventStore;
import io.streamliner.eventsourcing.processor.AbstractProcessor;
import io.streamliner.shopping.protocol.Command;
import io.streamliner.shopping.protocol.Event;
import io.streamliner.shopping.service.orderinterface.protocol.command.ConfirmOrder;
import io.streamliner.shopping.service.orderinterface.protocol.command.RejectOrder;
import io.streamliner.shopping.service.orderinterface.protocol.command.ValidateOrder;
import io.streamliner.shopping.service.orderinterface.protocol.event.OrderConfirmed;
import io.streamliner.shopping.service.orderinterface.protocol.event.OrderRejected;
import io.streamliner.shopping.service.orderinterface.protocol.event.OrderValidated;
import io.streamliner.shopping.service.orderinterface.protocol.state.OrderState;

import java.rmi.server.UID;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Processor extends AbstractProcessor<Command, Event> {

    private final String persistenceId;
    private final Map<String, OrderState> states;

    public Processor(final EventStore<Event> store, final String persistenceId) {
        super(store);
        this.persistenceId = persistenceId;
        this.states = new HashMap<>();
    }

    @Override
    public String getPersistenceId() {

        return persistenceId;
    }

    @Override
    protected void reply(final Envelope<Event> envelope) {

        final Event event = envelope.getValue();
        if (event instanceof OrderValidated) {
            final OrderState state = update((OrderValidated) event);
            states.put(state.getId(), state);
        } else if (event instanceof OrderConfirmed) {
            final OrderState state = update((OrderConfirmed) event);
            states.put(state.getId(), state);
        } else if (event instanceof OrderRejected) {
            final OrderState state = update((OrderRejected) event);
            states.remove(state.getId());
        } else {

        }
    }

    @Override
    public void receive(final Sender sender, final Command command) {

        if (command instanceof ValidateOrder) {
            receive(sender, (ValidateOrder) command);
        } else if (command instanceof ConfirmOrder) {
            receive(sender, (ConfirmOrder) command);
        } else if (command instanceof RejectOrder) {
            receive(sender, (RejectOrder) command);
        } else {

        }
    }

    private void receive(final Sender sender, final ValidateOrder command) {

        final OrderValidated event = new OrderValidated();
        event.setId(new UID().toString());
        event.setSku(command.getSku());
        event.setQuantity(command.getQuantity());
        event.setPrice(command.getPrice());
        event.setPaymentDetail(command.getPaymentDetail());
        event.setCustomerId(command.getCustomerId());
        event.setCheckoutId(command.getCheckoutId());
        event.setTimestamp(System.currentTimeMillis());
        persist(event, (transaction, eventEnvelope) -> {
            final OrderState state = update(event);
            transaction.commit(state.getId(), state, stateEnvelope -> {
                states.put(state.getId(), state);
                sender.reply(event);
            });
        });
    }

    private void receive(final Sender sender, final ConfirmOrder command) {

        final OrderState existingState = states.get(command.getId());
        final OrderConfirmed event = new OrderConfirmed();
        event.setId(command.getId());
        event.setCheckoutId(existingState.getCheckoutId());
        event.setPaymentId(command.getPaymentId());
        event.setTimestamp(System.currentTimeMillis());
        persist(event, (transaction, eventEnvelope) -> {
            final OrderState state = update(event);
            transaction.commit(state.getId(), state, stateEnvelope -> {
                states.put(state.getId(), state);
                sender.reply(event);
            });
        });
    }

    private void receive(final Sender sender, final RejectOrder command) {

        final OrderRejected event = new OrderRejected();
        event.setId(command.getId());
        event.setReason(command.getReason());
        event.setTimestamp(System.currentTimeMillis());
        persist(event, (transaction, eventEnvelope) -> {
            final OrderState state = update(event);
            transaction.commit(state.getId(), state, stateEnvelope -> {
                states.remove(state.getId());
                sender.reply(event);
            });
        });
    }

    private OrderState update(final OrderValidated event) {
        final OrderState state = new OrderState();
        state.setId(event.getId());
        state.setSku(event.getSku());
        state.setQuantity(event.getQuantity());
        state.setPrice(event.getPrice());
        state.setPaymentDetail(event.getPaymentDetail());
        state.setCustomerId(event.getCustomerId());
        state.setCheckoutId(event.getCheckoutId());
        state.setStatus("VALIDATED");
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

    private OrderState update(final OrderConfirmed event) {
        final OrderState state = new OrderState();
        state.setId(event.getId());
        state.setPaymentId(Optional.of(event.getPaymentId()));
        state.setStatus("CONFIRMED");
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

    private OrderState update(final OrderRejected event) {
        final OrderState state = new OrderState();
        state.setId(event.getId());
        state.setStatus("REJECTED");
        state.setTimestamp(System.currentTimeMillis());
        return state;
    }

}
