package io.streamliner.shopping.service.orderinterface;

import io.streamliner.shopping.protocol.Command;
import io.streamliner.shopping.service.checkout.protocol.event.CheckoutRequested;
import io.streamliner.shopping.service.checkout.protocol.model.PaymentDetail;
import io.streamliner.shopping.service.orderinterface.protocol.command.ConfirmOrder;
import io.streamliner.shopping.service.orderinterface.protocol.command.RejectOrder;
import io.streamliner.shopping.service.orderinterface.protocol.command.ValidateOrder;
import io.streamliner.shopping.service.payment.protocol.event.PaymentProcessed;
import io.streamliner.shopping.service.payment.protocol.event.PaymentRejected;

public class Adapter {

    public Command receive(final CheckoutRequested event) {

        final ValidateOrder command = new ValidateOrder();
        command.setSku(event.getSku());
        command.setQuantity(event.getQuantity());
        command.setPrice(event.getPrice());
        command.setCustomerId(event.getCustomerId());
        command.setCheckoutId(event.getId());
        // TODO: Set payment details
        //command.setPaymentDetail(event.getPaymentDetail());
        command.setTimestamp(event.getTimestamp());
        return command;
    }

    public Command receive(final PaymentRejected event) {

        final RejectOrder command = new RejectOrder();
        command.setId(event.getOrderId());
        command.setReason(event.getReason());
        command.setTimestamp(event.getTimestamp());
        return command;
    }

    public Command receive(final PaymentProcessed event) {

        final ConfirmOrder command = new ConfirmOrder();
        command.setId(event.getOrderId());
        command.setPaymentId(event.getId());
        command.setTimestamp(event.getTimestamp());
        return command;
    }

}
