package io.streamliner.shopping.service.orderinterface.protocol.event;

import io.streamliner.shopping.protocol.Event;

public class OrderRejected implements Event {

    private String id;
    private String reason;
    private String checkoutId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getReason() {
        return reason;
    }

    public String getCheckoutId() {
        return checkoutId;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
