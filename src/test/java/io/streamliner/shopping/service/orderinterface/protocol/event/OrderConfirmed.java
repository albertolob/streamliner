package io.streamliner.shopping.service.orderinterface.protocol.event;

import io.streamliner.shopping.protocol.Event;

public class OrderConfirmed implements Event {

    private String id;
    private String checkoutId;
    private String paymentId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getCheckoutId() {
        return checkoutId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
