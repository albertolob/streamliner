package io.streamliner.shopping.service.orderinterface.protocol.command;

import io.streamliner.shopping.protocol.Command;
import io.streamliner.shopping.service.orderinterface.protocol.model.PaymentDetail;

public class ValidateOrder implements Command {

    private String sku;
    private int quantity;
    private double price;
    private String customerId;
    private PaymentDetail paymentDetail;
    private String checkoutId;
    private long timestamp;

    public void setSku(String sku) {
        this.sku = sku;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public void setPaymentDetail(PaymentDetail paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public void setCheckoutId(String checkoutId) {
        this.checkoutId = checkoutId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSku() {
        return sku;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public String getCustomerId() {
        return customerId;
    }

    public PaymentDetail getPaymentDetail() {
        return paymentDetail;
    }

    public String getCheckoutId() {
        return checkoutId;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
