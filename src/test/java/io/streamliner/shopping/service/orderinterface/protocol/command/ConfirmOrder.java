package io.streamliner.shopping.service.orderinterface.protocol.command;

import io.streamliner.shopping.protocol.Command;

public class ConfirmOrder implements Command {

    private String id;
    private String paymentId;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
