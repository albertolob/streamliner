package io.streamliner.shopping.service.orderinterface.protocol.command;

import io.streamliner.shopping.protocol.Command;

public class RejectOrder implements Command {

    private String id;
    private String reason;
    private long timestamp;

    public void setId(String id) {
        this.id = id;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public String getReason() {
        return reason;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
